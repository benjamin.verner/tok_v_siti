using System.Reflection;
using System;
using Xunit;

namespace FlowNetworkTests;

public class GraphTest {
    [Fact]
    public void CorrectGraphTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[8];
        edges[0] = new Tuple<int, int, float>(1, 3, 1);
        edges[1] = new Tuple<int, int, float>(1, 4, 2);
        edges[2] = new Tuple<int, int, float>(1, 5, 3);
        edges[3] = new Tuple<int, int, float>(2, 3, 4);
        edges[4] = new Tuple<int, int, float>(2, 4, 5);
        edges[5] = new Tuple<int, int, float>(2, 5, 6);
        edges[6] = new Tuple<int, int, float>(3, 6, 7);
        edges[7] = new Tuple<int, int, float>(4, 6, 8);

        try {
            Graph.FlowNetwork testGraph = new Graph.FlowNetwork(6, edges);
        } catch {
            Assert.False(true, "Graph is correct and should pass");
        }

        Assert.True(true);
    }

    [Fact]
    public void NegativeCapacityTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[4];
        edges[0] = new Tuple<int, int, float>(1, 1, -1);
        edges[1] = new Tuple<int, int, float>(1, 2, -2);
        edges[2] = new Tuple<int, int, float>(1, 3, -3);
        edges[3] = new Tuple<int, int, float>(1, 4, -4);

        Assert.Throws<Exception>(() => { Graph.FlowNetwork testGraph = new Graph.FlowNetwork(4, edges); });
    }

    [Fact]
    public void CycleGraphTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[4];
        edges[0] = new Tuple<int, int, float>(1, 2, 1);
        edges[1] = new Tuple<int, int, float>(2, 3, 1);
        edges[2] = new Tuple<int, int, float>(3, 4, 1);
        edges[3] = new Tuple<int, int, float>(4, 1, 1);

        Assert.Throws<Exception>(() => { Graph.FlowNetwork testGraph = new Graph.FlowNetwork(4, edges); } );        
    }

    [Fact]
    public void NoSourcesTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[1];
        edges[0] = new Tuple<int, int, float>(1, 1, 1);

        Assert.Throws<Exception>(() => { Graph.FlowNetwork testGraph = new Graph.FlowNetwork(1, edges); } );
    }

    [Fact]
    public void NoSinksTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[4];
        edges[0] = new Tuple<int, int, float>(1, 2, 1);
        edges[1] = new Tuple<int, int, float>(1, 3, 1);
        edges[2] = new Tuple<int, int, float>(2, 3, 1);
        edges[3] = new Tuple<int, int, float>(3, 2, 1);

        Assert.Throws<Exception>(() => { Graph.FlowNetwork testgraph = new Graph.FlowNetwork(3, edges); } );
    }

    [Fact]
    public void ComplicatedGraphTest() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[10];
        edges[0] = new Tuple<int, int, float>(1, 2, 16);
        edges[1] = new Tuple<int, int, float>(1, 3, 13);
        edges[2] = new Tuple<int, int, float>(2, 3, 10);
        edges[3] = new Tuple<int, int, float>(2, 4, 12);
        edges[4] = new Tuple<int, int, float>(3, 2, 4);
        edges[5] = new Tuple<int, int, float>(3, 5, 14);
        edges[6] = new Tuple<int, int, float>(4, 6, 20);
        edges[7] = new Tuple<int, int, float>(4, 3, 9);
        edges[8] = new Tuple<int, int, float>(5, 4, 7);
        edges[9] = new Tuple<int, int, float>(5, 6, 4);

        try {
            Graph.FlowNetwork testGraph = new Graph.FlowNetwork(6, edges);
        } catch {
            Assert.False(true, "Graph is correct and should pass");
        }
    }
}