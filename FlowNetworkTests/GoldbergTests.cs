namespace FlowNetworkTests;

public partial class GoldbergTests {
    [Fact]
    public void AllEdgesTest() {
        Graph.FlowNetwork testGraph = TestGraphs.AllEdgesTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Goldberg(testGraph);
        TestGraphs.CheckAllEdges(testGraph, maximumFlow);
    }

    [Fact]
    public void SimpleFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.SimpleFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Goldberg(testGraph);
        TestGraphs.CheckSimpleFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void ComplicatedFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.ComplicatedFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Goldberg(testGraph);
        TestGraphs.CheckComplicatedFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void TrickTest() {
        Graph.FlowNetwork testGraph = TestGraphs.TrickTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Goldberg(testGraph);
        TestGraphs.CheckTrick(testGraph, maximumFlow);
    }
}