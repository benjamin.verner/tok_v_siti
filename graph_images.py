#!/usr/bin/env python3

from dsplot.graph import Graph

def all_edges_test_graph():
    graph = Graph(
        {1: [2, 3], 2: [3, 4], 3: [4], 4: []},
        directed = True,
        edges = {'12': 3, '13': 2, '23': 5, '24': 2, '34': 3},
    )
    graph.plot('./all_edges.png')

def simple_flow_test_graph():
    graph = Graph(
        {1: [2, 3], 2: [3, 4], 3: [4], 4: []},
        directed = True,
        edges = {'12': 1, '13': 1, '23': 1, '24': 1, '34': 1},
    )
    graph.plot('./simple_flow.png')

def complicated_flow_test_graph():
    graph = Graph(
        {1: [2, 3], 2: [3, 4], 3: [2, 5], 4: [3, 6], 5: [4, 6], 6: []},
        directed = True,
        edges = {'12': 16, '13': 13, '23': 10, '24': 12, '32': 4, '35': 14, '43': 9, '46': 20, '54': 7, '56': 4},
    )
    graph.plot('./complicated_flow.png')

def trick_test_graph():
    graph = Graph(
        {1: [2, 5], 2: [3], 3: [4], 4: [8], 5: [4, 6], 6: [7], 7: [8], 8: [9], 9: []},
        directed = True,
        edges = {'12': 1, '15': 1, '23': 1, '34': 1, '48': 1, '54': 1, '56': 1, '67': 1, '78': 1, '89': 2},
    )
    graph.plot('./trick_test.png')

def main():
    all_edges_test_graph()
    simple_flow_test_graph()
    complicated_flow_test_graph()
    trick_test_graph()

if __name__=='__main__':
    main()