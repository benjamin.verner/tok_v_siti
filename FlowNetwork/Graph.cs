﻿namespace Graph {
    public class FlowNetwork {

        public class Edge {
            /*
            Class representing edges in the adjacency list
             - edge is oriented, but algorithms use it both ways
             - included in the adjacency list of both the starting and the ending vertice
             - methods taking '''int from''' as a parameter are working differently depending on whether the algorithm
             sees them from the starting or the ending vertice
            */
            public Edge(int From, int To, float value) {
                this.From = From;
                this.To = To;
                this.Capacity = value;
                this.Flow = 0;
            }

            public bool isForward(int from) {
                return this.From == from;
            }

            public float getResidualCapacity(int from) {
                /*
                how many units of flow can be sent this way
                 - if forward, only the remaining capacity can be sent
                 - if backward, only the current flow can be subtracked
                */
                if (from == this.From) {
                    return this.Capacity - this.Flow;
                } else {
                    return this.Flow;
                }
            }

            public float getResidualCapacity() {
                /*
                get forward residual capacity
                */
                return this.Capacity - this.Flow;
            }

            public int getTo(int from) {
                /*
                get ending vertice from the given point of view
                */
                if (from == this.From) {
                    return this.To;
                } else {
                    return this.From;
                }
            }

            public void updateFlow(int from, float value) {
                if (from == this.From) {
                    this.Flow += value;
                } else {
                    this.Flow -= value;
                }
            }

            public int From { get; }
            public int To { get; }
            public float Capacity { get; set; }
            public float Flow { get; set; }
        }

        public List<Edge>[] adjacencyList;
        public int numOfVertices;

        private static bool isInRange(int index, int numOfVertices) {
            /*
            Returns whether the node index is valid
            */

            return index > 0 && index <= numOfVertices;
        }

        private static void findSourcesAndSinks(List<Edge>[] adjacencyList, out List<int> sources, out List<int> sinks) {
            /*
            Method for finding sources and sinks of the graph From adjacency list
            */

            int[] inValue = new int[adjacencyList.Length];
            sources = new List<int>();

            int[] outValue = new int[adjacencyList.Length];
            sinks = new List<int>();

            for (int i = 0; i < inValue.Length; ++i) {
                inValue[i] = 0;
            }

            // for each edge: increment value in the ending node index of inValue
            //                increment value in the starting node index of outValue
            for (int i = 1; i < adjacencyList.Length - 1; ++i) {
                foreach (Edge edge in adjacencyList[i]) {
                    outValue[edge.From]++;
                    inValue[edge.To]++;
                }
            }

            // each node with zero in the inValue field is a source
            // each node with zero in the outValue field is a sink
            for (int i = 1; i < adjacencyList.Length - 1; ++i) {
                if (inValue[i] == 0) {
                    sources.Add(i);
                }
                if (outValue[i] == 0) {
                    sinks.Add(i);
                }
            }
        }

        public FlowNetwork(int numOfVertices, Tuple<int, int, float>[] edges) {
            /*
            numOfNodes:
                number of nodes in the graph
                node indexing is From 1 to numOfNodes
                    index 0 is reserved for MetaSource: a virtual node parenting each of the graph's sources
                    index numOfNodes + 1 is reserved for MetaSink: a virtual node being a child of each of the graph's sinks
                    (reserved nodes are needed for flow algorithms)

            edges:
                an array of tuples in form of <fromEdge, toEdge, edgeCapacity>
            */

            this.adjacencyList = new List<Edge>[numOfVertices + 2];
            this.numOfVertices = numOfVertices + 2;

            for (int i = 0; i < this.numOfVertices; ++i) {
                this.adjacencyList[i] = new List<Edge>();
            }

            // checks for validity of the input edges
            foreach (Tuple<int, int, float> edge in edges) {
                if (!(isInRange(edge.Item1, numOfVertices) && isInRange(edge.Item2, numOfVertices))) {
                    throw new Exception("Node index out of range");
                }

                if (edge.Item3 < 0) {
                    throw new Exception("Edge capacity must be a non-negative value");
                }

                Edge newEdge = new Edge(edge.Item1, edge.Item2, edge.Item3);
                this.adjacencyList[edge.Item1].Add(newEdge);
                this.adjacencyList[edge.Item2].Add(newEdge);
            }

            List<int> sources = new List<int>();
            List<int> sinks = new List<int>();

            findSourcesAndSinks(this.adjacencyList, out sources, out sinks);

            foreach (int source in sources) {
                // compute the sum of capacities of source edges
                float outletCapacity = 0;
                foreach (Edge edge in this.adjacencyList[source]) {
                    outletCapacity += edge.Capacity;
                }
                // connect all sources to the MetaSource
                Edge newEdge = new Edge(0, source, outletCapacity);
                this.adjacencyList[0].Add(newEdge);
                this.adjacencyList[source].Add(newEdge);
            }

            foreach (int sink in sinks) {
                // connect all sinks to the MetaSink
                Edge newEdge = new Edge(sink, this.numOfVertices - 1, float.MaxValue);
                this.adjacencyList[sink].Add(newEdge);
                this.adjacencyList[this.numOfVertices - 1].Add(newEdge);
            }

            if (sources.Count == 0) {
                throw new Exception("Invalid Flow Network: no source node detected");
            }

            if (sinks.Count == 0) {
                throw new Exception("Invalid Flow Network: no sink node detected");
            }
        }
        public static void Main(string[] args) {}
    }
}