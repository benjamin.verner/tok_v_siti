namespace FlowNetworkTests;

public class TestGraphs {
    public static Graph.FlowNetwork AllEdgesTestGraph() {
        Tuple<int, int, float>[] edges = new Tuple<int, int, float>[5];
        edges[0] = new Tuple<int, int, float>(1, 2, 3);
        edges[1] = new Tuple<int, int, float>(1, 3, 2);
        edges[2] = new Tuple<int, int, float>(2, 3, 5);
        edges[3] = new Tuple<int, int, float>(2, 4, 2);
        edges[4] = new Tuple<int, int, float>(3, 4, 3);

        Graph.FlowNetwork testGraph = new Graph.FlowNetwork(4, edges);
        return testGraph;
    }

    public static void CheckAllEdges(Graph.FlowNetwork testGraph, float maximumFlow) {
        Assert.Equal(5F, maximumFlow);

        foreach (Graph.FlowNetwork.Edge edge in testGraph.adjacencyList[1]) {
            if (edge.From == 1 && edge.To == 2) {
                Assert.Equal(3F, edge.Flow);
            } else if (edge.From == 1 && edge.To == 3) {
                Assert.Equal(2F, edge.Flow);
            }
        }
        foreach (Graph.FlowNetwork.Edge edge in testGraph.adjacencyList[2]) {
            if (edge.From == 2 && edge.To == 3) {
                Assert.Equal(1F, edge.Flow);
            } else if (edge.From == 2 && edge.To == 4) {
                Assert.Equal(2F, edge.Flow);
            }
        }
        foreach (Graph.FlowNetwork.Edge edge in testGraph.adjacencyList[3]) {
            if (edge.From == 3 && edge.To == 4) {
                Assert.Equal(3F, edge.Flow);
            }
        }
    }

    public static Graph.FlowNetwork SimpleFlowTestGraph() {
        Tuple<int, int, float>[] edges = new Tuple<int, int, float>[5];
        edges[0] = new Tuple<int, int, float>(1, 2, 1);
        edges[1] = new Tuple<int, int, float>(1, 3, 1);
        edges[2] = new Tuple<int, int, float>(2, 3, 1);
        edges[3] = new Tuple<int, int, float>(2, 4, 1);
        edges[4] = new Tuple<int, int, float>(3, 4, 1);

        Graph.FlowNetwork testGraph = new Graph.FlowNetwork(4, edges);
        return testGraph;
    }

    public static void CheckSimpleFlow(Graph.FlowNetwork testGraph, float maximumFlow) {
        Assert.Equal(2F, maximumFlow);
    }

    public static Graph.FlowNetwork ComplicatedFlowTestGraph() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[10];
        edges[0] = new Tuple<int, int, float>(1, 2, 16);
        edges[1] = new Tuple<int, int, float>(1, 3, 13);
        edges[2] = new Tuple<int, int, float>(2, 3, 10);
        edges[3] = new Tuple<int, int, float>(2, 4, 12);
        edges[4] = new Tuple<int, int, float>(3, 2, 4);
        edges[5] = new Tuple<int, int, float>(3, 5, 14);
        edges[6] = new Tuple<int, int, float>(4, 6, 20);
        edges[7] = new Tuple<int, int, float>(4, 3, 9);
        edges[8] = new Tuple<int, int, float>(5, 4, 7);
        edges[9] = new Tuple<int, int, float>(5, 6, 4);

        Graph.FlowNetwork testGraph = new Graph.FlowNetwork(6, edges);
        return testGraph;
    }

    public static void CheckComplicatedFlow(Graph.FlowNetwork testGraph, float maximumFlow) {
        Assert.Equal(23F, maximumFlow);
    }

    public static Graph.FlowNetwork TrickTestGraph() {
        Tuple<int, int, float>[]  edges = new Tuple<int, int, float>[9];
        edges[0] = new Tuple<int, int, float>(1, 2, 1);
        edges[1] = new Tuple<int, int, float>(2, 3, 1);
        edges[2] = new Tuple<int, int, float>(3, 4, 1);
        edges[3] = new Tuple<int, int, float>(1, 5, 1);
        edges[4] = new Tuple<int, int, float>(5, 4, 1);
        edges[5] = new Tuple<int, int, float>(5, 6, 1);
        edges[6] = new Tuple<int, int, float>(6, 7, 1);
        edges[7] = new Tuple<int, int, float>(7, 8, 1);
        edges[8] = new Tuple<int, int, float>(4, 8, 1);

        Graph.FlowNetwork testGraph = new Graph.FlowNetwork(8, edges);
        return testGraph;
    }

    public static void CheckTrick(Graph.FlowNetwork testGraph, float maximumFlow) {
        Assert.Equal(2F, maximumFlow);
    }
}