using System.Timers;
using System.Reflection.Metadata.Ecma335;
namespace MaximumFlow {
    public partial class Algorithms {
        private static List<Graph.FlowNetwork.Edge>[] ResidualGraph(Graph.FlowNetwork graph) {
            /*
            creates a residual graph from the given flow network
            (residual graph includes only edges with residual capacity greater than 0)
            */

            List<Graph.FlowNetwork.Edge>[] residualGraph = new List<Graph.FlowNetwork.Edge>[graph.numOfVertices];

            for (int i = 0; i < graph.numOfVertices; ++i) {
                residualGraph[i] = new List<Graph.FlowNetwork.Edge>();
            }

            // loop through all edges
            for (int i = 0; i < graph.numOfVertices; ++i) {
                foreach (Graph.FlowNetwork.Edge edge in graph.adjacencyList[i]) {
                    // add only edges with residual capacity greater than 0
                    if (edge.getResidualCapacity(i) > 0) {
                        residualGraph[i].Add(edge);
                    }
                }
            }

            return residualGraph;
        }

        private static bool ShortestPath(List<Graph.FlowNetwork.Edge>[] residualGraph, out int[] distances) {
            /*
            BFS Algorithm, returns if sink was reached
            out parameter distances stores the level of each vertice (distance from source)
            */

            distances = new int[residualGraph.Length];

            for (int i = 0; i < distances.Length; ++i) {
                distances[i] = -1;
            }

            distances[0] = 0;
            Queue<int> bfsQueue = new Queue<int>();
            bfsQueue.Enqueue(0);

            // BFS search with queue
            while (bfsQueue.Count != 0) {
                int location = bfsQueue.Dequeue();

                foreach (Graph.FlowNetwork.Edge edge in residualGraph[location]) {
                    // if ending vertice wasn't visited yet
                    if (distances[edge.getTo(location)] == -1) {
                        // update it's distance to 1 + the current distance and add it to the queue
                        distances[edge.getTo(location)] = distances[location] + 1;
                        bfsQueue.Enqueue(edge.getTo(location));
                    }
                }
            }

            int length = distances[distances.Length - 1];

            if (length == -1) {
                return false;
            }

            return true;
        }

        private static void NetworkCleaning(List<Graph.FlowNetwork.Edge>[] residualGraph, int[] distances) {
            /*
            cleans up all edges not on the shortest path from source to sink
            */

            for (int i = 0; i < residualGraph.Length; ++i) {
                for (int j = residualGraph[i].Count - 1; j >= 0; --j) {
                    // remove all non forward edges and edges to layer greater than the sink layer
                    // using backwards for loop instead of foreach loop so that edges can be removed without disturbing the loop
                    // current edge is residualGraph[i][j]

                    if (distances[residualGraph[i][j].getTo(i)] > distances[residualGraph.Length - 1]
                    || distances[i] >= distances[residualGraph[i][j].getTo(i)]) {
                        residualGraph[i].RemoveAt(j);
                    }
                }
            }
        }

        private static float DFS(List<Graph.FlowNetwork.Edge>[] residualGraph, float maxFlow = float.MaxValue, int vertice = 0) {
            /*
            DFS Algorithm finding a path in the level graph
            if a path is found, updates flows of path edges and remove them from the graph if their residual capacity is 0
            if backtracking occurs, the vertice is a dead end - the algorithm removes all its edges and the edge coming to it
            */

            if (vertice == residualGraph.Length - 1) {
                return maxFlow;
            }

            // Depth First Search
            for (int i = residualGraph[vertice].Count - 1; i >= 0; --i) {
                // using backwards for loop instead of foreach loop so that edges can be removed without exception
                // current edge is residualGraph[vertice][i]

                float flow = Math.Min(maxFlow, residualGraph[vertice][i].getResidualCapacity(vertice));
                flow = Math.Min(flow, DFS(residualGraph, flow, residualGraph[vertice][i].getTo(vertice)));

                if (flow != 0) {
                    // algorithm reached the sink and returned a non zero value
                    // update the edge flow
                    residualGraph[vertice][i].updateFlow(vertice, flow);

                    // remove the edge from the residual graph if the edge capacity is reached
                    if (residualGraph[vertice][i].getResidualCapacity(i) == 0) {
                        residualGraph[vertice].RemoveAt(i);
                    }

                    // return the flow (for the previous layers to be updated)
                    return flow;

                } else {
                    // algorithm didn't reach the sink, backtracking is needed and this edge is a dead end
                    residualGraph[vertice].RemoveAt(i);
                }

            }

            // Sink not reached, backtracking is needed
            return 0;
        }

        private static float BlockingFlow(List<Graph.FlowNetwork.Edge>[] residualGraph) {
            /*
            runs DFS while there is still a path from source to sink in the level graph
            */

            float flow = 0;
            while (true) {
                float addition = DFS(residualGraph);
                if (addition == 0) {
                    return flow;
                }
                flow += addition;
            }
        }

        public static float Dinic(Graph.FlowNetwork graph) {
            int[] distances;
            float maxFlow = 0;

            while (true) {
                List<Graph.FlowNetwork.Edge>[] residualGraph = ResidualGraph(graph);

                if (!ShortestPath(residualGraph, out distances)) {
                    return maxFlow;
                }

                NetworkCleaning(residualGraph, distances);

                maxFlow += BlockingFlow(residualGraph);
            }
        }
    }
}