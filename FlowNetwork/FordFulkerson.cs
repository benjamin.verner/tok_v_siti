namespace MaximumFlow {
    public partial class Algorithms {

        private static bool FlowBFS(Graph.FlowNetwork graph, out Queue<int> path, out List<Graph.FlowNetwork.Edge> pathEdges, out float pathFlow) {
            /*
            BFS search in the graph
            returns if there exists a path from source to sink
            out parameters:
                path - queue of vertices of the path in order
                pathEdges - list of edges of the path in order
                pathFlow - flow that can be sent through the path
            */

            // used for backtracking the path
            int[] ancestors = new int[graph.numOfVertices];

            // used to retrieve path edges
            Graph.FlowNetwork.Edge[] ancestorEdges = new Graph.FlowNetwork.Edge[graph.numOfVertices];

            // used for retrieving path residual capacity
            float[] edgeResidual = new float[graph.numOfVertices];

            for (int i = 0; i < graph.numOfVertices; ++i) {
                ancestors[i] = -1;
                edgeResidual[i] = 0;
            }

            Queue<int> bfsQueue = new Queue<int>();
            bfsQueue.Enqueue(0);

            // BFS search with queue
            while (bfsQueue.Count != 0) {
                int location = bfsQueue.Dequeue();

                foreach (Graph.FlowNetwork.Edge edge in graph.adjacencyList[location]) {
                    // if residual capacity is > 0 and the neighbor was not visited yet
                    if (edge.getResidualCapacity(location) > 0 && ancestors[edge.getTo(location)] == -1) {
                        bfsQueue.Enqueue(edge.getTo(location));
                        ancestors[edge.getTo(location)] = location;
                        ancestorEdges[edge.getTo(location)] = edge;
                        edgeResidual[edge.getTo(location)] = edge.getResidualCapacity(location);
                    }
                }
            }

            path = new Queue<int>();
            pathEdges = new List<Graph.FlowNetwork.Edge>();
            pathFlow = 0;

            // returns false if no path reached the stock
            if (ancestors[graph.numOfVertices - 1] == -1) {
                return false;
            }

            // reconstruct the path from the end
            int pathBack = graph.numOfVertices - 1;
            pathFlow = edgeResidual[pathBack];

            while (pathBack != 0){
                pathEdges.Add(ancestorEdges[pathBack]);
                path.Enqueue(ancestors[pathBack]);

                // update path residual capacity
                pathFlow = Math.Min(pathFlow, edgeResidual[pathBack]);

                pathBack = ancestors[pathBack];
            }

            return true;
        }

        public static float FordFulkerson(Graph.FlowNetwork graph) {
            float maximumFlow = 0;

            List<Graph.FlowNetwork.Edge> pathEdges = new List<Graph.FlowNetwork.Edge>();
            Queue<int> path = new Queue<int>();
            float pathFlow;

            // while there exists a path that can be improved
            while (FlowBFS(graph, out path, out pathEdges, out pathFlow)) {
                // update the flow of every path edge
                foreach (Graph.FlowNetwork.Edge edge in pathEdges) {
                    int from = path.Dequeue();
                    edge.updateFlow(from, pathFlow);
                }

                // overall flow is increased by the flow the path was increased by
                maximumFlow += pathFlow;
            }

            return maximumFlow;
        }
    }
}