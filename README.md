# Tok v Síti

Tato práce se zabývá toky v sı́tı́ch, konkrétně problematikou hledánı́ největšı́ho toku v sı́ti, a implementacı́ jednotlivých známých algoritmů na toto téma v jazyku C#. Implementovány jsou algoritmy Fordův-Fulkersonův, Dinicův a Goldbergův-Tarjanův.
