namespace FlowNetworkTests;

public class DinicTests {
    [Fact]
    public void AllEdgesTest() {
        Graph.FlowNetwork testGraph = TestGraphs.AllEdgesTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Dinic(testGraph);
        TestGraphs.CheckAllEdges(testGraph, maximumFlow);
    }

    [Fact]
    public void SimpleFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.SimpleFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Dinic(testGraph);
        TestGraphs.CheckSimpleFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void ComplicatedFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.ComplicatedFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Dinic(testGraph);
        TestGraphs.CheckComplicatedFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void TrickTest() {
        Graph.FlowNetwork testGraph = TestGraphs.TrickTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.Dinic(testGraph);
        TestGraphs.CheckTrick(testGraph, maximumFlow);
    }
}