namespace FlowNetworkTests;

public partial class FordFulkersonTests {
    [Fact]
    public void AllEdgesTest() {
        Graph.FlowNetwork testGraph = TestGraphs.AllEdgesTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.FordFulkerson(testGraph);
        TestGraphs.CheckAllEdges(testGraph, maximumFlow);
    }

    [Fact]
    public void SimpleFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.SimpleFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.FordFulkerson(testGraph);
        TestGraphs.CheckSimpleFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void ComplicatedFlowTest() {
        Graph.FlowNetwork testGraph = TestGraphs.ComplicatedFlowTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.FordFulkerson(testGraph);
        TestGraphs.CheckComplicatedFlow(testGraph, maximumFlow);
    }

    [Fact]
    public void TrickTest() {
        Graph.FlowNetwork testGraph = TestGraphs.TrickTestGraph();
        float maximumFlow = MaximumFlow.Algorithms.FordFulkerson(testGraph);
        TestGraphs.CheckTrick(testGraph, maximumFlow);
    }
}