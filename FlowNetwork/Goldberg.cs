namespace MaximumFlow {
    public partial class Algorithms {
        public static float Goldberg(Graph.FlowNetwork graph) {
            // vertice heights
            int[] height = new int[graph.numOfVertices];
            height[0] = graph.numOfVertices;
            for (int i = 1; i < height.Length; ++i) {
                height[i] = 0;
            }

            // array of overflow size for each vertice
            float[] overflow = new float[graph.numOfVertices];
            for (int i = 0; i < graph.numOfVertices; ++i) {
                overflow[i] = 0;
            }

            Queue<int> selectionQueue = new Queue<int>();

            // number of overflowed vertices
            int overflowNum = 0;

            // List of all edges going down
            List<Graph.FlowNetwork.Edge>[] downEdges = new List<Graph.FlowNetwork.Edge>[graph.numOfVertices];
            for (int i = 0; i < downEdges.Length; ++i) {
                downEdges[i] = new List<Graph.FlowNetwork.Edge>();
            }

            // create initial wave
            // (raise the flow of all edges from the source to their capacities and update overflow information)
            foreach (Graph.FlowNetwork.Edge edge in graph.adjacencyList[0]) {
                if (edge.Capacity > 0) {
                    edge.Flow = edge.Capacity;
                    overflowNum++;
                    overflow[edge.To] += edge.Flow;
                    selectionQueue.Enqueue(edge.To);
                }
            }

            // index of the vertice the algorithm is working with
            int nextOverflowVertice = selectionQueue.Dequeue();

            // main algorithm loop
            while (overflowNum != 0) {
                // remove edges which aren't going down
                while (downEdges[nextOverflowVertice].Count != 0 && height[nextOverflowVertice] <= height[downEdges[nextOverflowVertice].Last().getTo(nextOverflowVertice)]) {
                    downEdges[nextOverflowVertice].RemoveAt(downEdges[nextOverflowVertice].Count - 1);
                }

                // if there are down edges through which we can send some flow
                if (downEdges[nextOverflowVertice].Count != 0) {
                    // substitution for better code readability
                    // transfer as much flow as possible through the edge
                    int neighborVertice = downEdges[nextOverflowVertice].Last().getTo(nextOverflowVertice);
                    float edgeCapacity = downEdges[nextOverflowVertice].Last().getResidualCapacity(nextOverflowVertice);

                    float transferableOverflow = Math.Min(overflow[nextOverflowVertice], edgeCapacity);

                    downEdges[nextOverflowVertice].Last().updateFlow(nextOverflowVertice, transferableOverflow);
                    overflow[nextOverflowVertice] -= transferableOverflow;



                    // if the next vertice isn't the source or the sink and we transferred something
                    if (neighborVertice != 0 && neighborVertice != graph.numOfVertices - 1 && transferableOverflow != 0) {
                        // update overflowNum and overflow values
                        if (overflow[neighborVertice] == 0) { overflowNum++; selectionQueue.Enqueue(neighborVertice); }
                        overflow[neighborVertice] += transferableOverflow;
                    }

                    // decrease overflowNum if we transferred all the flow needed from this vertice
                    // else add it back to the queue
                    if (overflow[nextOverflowVertice] == 0) { overflowNum--; }
                    else { selectionQueue.Enqueue(nextOverflowVertice); }
                    
                    // remove the edge from the list if its capacity becomes 0
                    if (edgeCapacity == 0) { downEdges[nextOverflowVertice].RemoveAt(downEdges[nextOverflowVertice].Count - 1); }

                    // get next overflown vertice
                    if (selectionQueue.Count != 0) {
                        nextOverflowVertice = selectionQueue.Dequeue();
                    }

                } else {
                    // lift up the vertice
                    height[nextOverflowVertice] += 1;

                    // update its down edges
                    foreach (Graph.FlowNetwork.Edge edge in graph.adjacencyList[nextOverflowVertice]) {
                        if (edge.getResidualCapacity(nextOverflowVertice) > 0) {
                            if (height[nextOverflowVertice] > height[edge.getTo(nextOverflowVertice)]) {
                                downEdges[nextOverflowVertice].Add(edge);
                            }
                        }
                    }
                }
            }

            // compute the final flow
            float maxFlow = 0;
            foreach (Graph.FlowNetwork.Edge edge in graph.adjacencyList[0]) {
                maxFlow += edge.Flow;
            }

            return maxFlow;
        }
    }
}